    function openAddForm() {
    document.getElementById("addForm").style.display = "block";
}

    function closeAddForm() {
    document.getElementById("addForm").style.display = "none";
}
    function addStudent(){
        var table = document.getElementById("myTable");
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        var cellCount = table.rows[0].cells.length;
        for(var i = 0; i < cellCount; i++){
            var cell = row.insertCell(i);
            switch (i){
                case 0:
                    cell.innerHTML = '<th><input type = checkbox class = "table_checkbox"></th>';
                    break;
                case 1:
                    cell.innerHTML = '<th>PZ-25</th>';
                    break;
                case 2:
                    cell.innerHTML = '<th>Viktor</th>';
                    break;
                case 3:
                    cell.innerHTML = '<th>M</th>';
                    break;
                case 4:
                    cell.innerHTML = '<th>06.11.2003</th>';
                    break;
                case 5:
                    cell.innerHTML = '<input type="radio"/>';
                    break;
                case 6:
                    cell.innerHTML = '<button> <img src = ./images/editButt.png  style="max-height:30px; max-width:30px" /> <button onclick="deleteRow(this)"> <img src = ./images/delButt.png  style="max-height:30px; max-width:30px" />';
                    break;

            }

        }
        openAddForm();
}

    function deleteRow(ele){
        var table = document.getElementById('myTable');
        var rowCount = table.rows.length;
        if(rowCount <= 1){
            alert("There is no row available to delete!");
            return;
        }
        if(ele){
            //delete specific row
            ele.parentNode.parentNode.remove();
        }else{
            //delete last row
            table.deleteRow(rowCount-1);
        }
    }
