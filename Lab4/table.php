<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="styles.css" />


    <title>Lab 3</title>
</head>
<body>
<ul class="topMenu">
    <li><a href="https://lpnu.ua/">CMS</a></li>
    <div class="dropdown">
        <button class="drop-source">
            <img src="./images/profilePicture.png" class="profile" />Hladun Viktor
        </button>
        <div class="dropdown-content">
            <a>Profile</a>
            <a>Log Out</a>
        </div>
    </div>
    <div class="dropdown">
        <button class="drop-source">
            <img src="./images/bell.png" class="profile"/>
            <div class="circle-anim"></div>
        </button>
        <div class="dropdown-content">
            <a>LNU has fallen. Millions must Polytechnize</a>
            <a>Notice : You were added to group Repeated Learning 2023</a>
            <a>Warning! The fog is coming</a>
        </div>
    </div>
</ul>
<h1 class="title">Students</h1>
<main class="main">

    <button class="AddButton" >&plus;</button>
    <div class="table_wrapper">
        <table id = "myTable" class="mainTable">
            <thead> <tr>
                <th><input type = checkbox class = "table_checkbox"></th>
                <th>Group</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Birthday</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $conn = new mysqli("localhost", "root", "","pvi_lab4");

            $sql = "SELECT * from students";
            $result = $conn->query($sql);

            if(!$result) { die ("Invalid query : " . $conn->error);}
            while($row = $result->fetch_assoc()){
               echo "
                <tr>
                <th><input type = checkbox class = 'table_checkbox'></th>
                <th>" . $row["GroupStudent"] . " </th>
                <th>" . $row["NameStudent"] . " </th>
                <th>" . $row["Gender"] . " </th>
                <th>" . $row["Birthday"] . " </th>
                <th><input type='radio'/></th>
                <th> 
                <div class='option-buttons'>
                <button class='btn--edit'>
                  <img src = ./images/editButt.png  style='max-height:30px; max-width:30px' onclick='openEditStudentWindow(this)' /> 
                </button>
                <button class='btn--delete'>
                 <img src = ./images/delButt.png  style='max-height:30px; max-width:30px' onclick='openDeleteStudentWindow(this)' />
                </button>
              </div>
              </th>
              <th style='display: none' id='tableStudID' >". $row["ID"] . "</th>
              </tr>";
            }
            $conn->close();
            ?>
            </tbody>
        </table>

    </div>
    <div class=" Tabs_wrapper">
        <ul class="Tabs_List">
            <li>Dashboard</li>
            <li><b>Students</b></li>
            <li>Tasks</li>
        </ul>
    </div>
    <ul class="Pages_List">
        <li> > </li>
        <li> 4 </li>
        <li> 3 </li>
        <li> 2 </li>
        <li> 1 </li>
        <li> < </li>
    </ul>
    <div class="AddStudentWindow">
        <div class="AddWindowContent">
            <header class="AddWindowHeader">
                <h3 class="AddWindowHeading">Add student</h3>
            </header>
            <form
                action="server.php"
                method="post"
                id="AddStudentForm"
                class="addForm"
            >
                <div class="Add_inputContainer">
                    <label for="group" class="Add_formLabel">Group</label>
                    <select name="group" id="group" class="Add_input" required>
                        <option value="">Select group</option>
                        <option value="PZ-21">PZ-21</option>
                        <option value="PZ-22">PZ-22</option>
                        <option value="PZ-23">PZ-23</option>
                    </select>
                </div>
                <div class="Add_inputContainer">
                    <label for="first-name" class="Add_formLabel"
                    >First name</label
                    >
                    <input
                        name="first-name"
                        id="first-name"
                        type="text"
                        class="Add_input"
                        required
                    />
                </div>
                <div class="Add_inputContainer">
                    <label for="last-name" class="Add_formLabel"
                    >Last name</label
                    >
                    <input
                        name="last-name"
                        id="last-name"
                        type="text"
                        class="Add_input"
                        required
                    />
                </div>
                <div class="Add_inputContainer">
                    <label for="gender" class="Add_formLabel">Gender</label>
                    <select
                        name="gender"
                        id="gender"
                        class="Add_input"
                        required
                    >
                        <option value="">Select gender</option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
                <div class="Add_inputContainer">
                    <label for="birthday" class="Add_formLabel"
                    >Birthday</label
                    >
                    <input
                        name="birthday"
                        id="birthday"
                        type="date"
                        class="Add_input"
                        required
                    />
                </div>
                <div class="Add_Buttons">
                    <button class="AddFormButton add_submit" type="submit">
                        Submit
                    </button>
                    <button class="AddFormButton add_cancel" type="button">
                        Cancel
                    </button>
                </div>
                <input type="hidden" value=" -1 " id="StudID" name="StudID" >
            </form>
        </div>
    </div>
    <div class="delWindow">
        <div class="delWindow-content">
            <header class="delete-student-header">
                <h3 class="delHeader">Warning</h3>
            </header>
            <p class="delText">
                Are you sure you want to delete
                <span class="student-to-delete">User</span>?
            </p>
            <form action="server.php" method="post" class="delete student-form">
                <div class="form-buttons">
                    <button
                        name="submit-deletion"
                        class="AddFormButton delStudentSubmit"
                        type="submit"
                    >
                        Submit
                    </button>
                    <button
                        class="AddFormButton delStudentCancel"
                        type="button"
                    >
                        Cancel
                    </button>
                </div>
                <input type="hidden" value=" -1 " id="StudID"  name="StudID" >
            </form>
        </div>
    </div>

</main>



<script src="js/Lab3Script.js"></script>
</body>
</html>
