let rowToEdit;
let rowToDelete;
let idToDeleteEdit;

// ADD STUDENT

const btnAddStudent = document.querySelector(".AddButton");
const studentWindow = document.querySelector(".AddStudentWindow");

function openAddStudentWindow() {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Add student";
    form.querySelectorAll("input").forEach((input) => (input.value = ""));
    form.querySelectorAll("select").forEach((select) => (select.value = ""));
}

btnAddStudent.addEventListener("click", openAddStudentWindow);

const btnAddStudentForm = document.querySelector(".add_submit");
const form = document.querySelector(".addForm");
const tableContent = document.querySelector(".mainTable");

function addStudent(e) {
    let isFormValid = form.checkValidity();
    if (!isFormValid) {
        form.reportValidity();
    } else {
        e.preventDefault();
        form.checkValidity();
        let studentAction;
        if (studentWindow.querySelector("h3").textContent == "Add student") {
            studentAction = "add";
        } else {
            studentAction = "edit";
        }

        let group, name, gender, birthday, id;
        group = form.querySelector('[name="group"]').value;
        name =
            form.querySelector('[name="first-name"]').value +
            " " +
            form.querySelector('[name="last-name"]').value;

        if (!name.trim( )) {
            name = " ";

        }
        gender = form.querySelector('[name="gender"]').value;
        birthday = form.querySelector('[name="birthday"]').value;
        id = form.querySelector('[name="StudID"]').value;

        var xhr = new XMLHttpRequest();
        var url = "server.php";
        var data = new FormData();
        data.append("action", studentAction);
        data.append("group", group);
        data.append("name", name);
        data.append("gender", gender);
        data.append("birthday", birthday);
        data.append("ID",id);
        xhr.open("POST", url, true);
        xhr.onload = function () {
            if (xhr.status === 200) {
                console.log(xhr.responseText);
                var response = JSON.parse(xhr.responseText);

                if (response.status === "success") {
                    if (studentAction === "add") {
                        tableContent.insertAdjacentHTML(
                            "beforeend",
                            `
          <tr>
            <td>
              <input type = checkbox class = "table_checkbox">
            </td>
            <td>
              ${response.group}
            </td>
            <td>
              ${response.name}
            </td>
            <td>
              ${response.gender}
            </td>
            <td>
              ${response.birthday}
            </td>
            <td>
              <input type="radio"/>
            </td>
            <td>
              <div class="option-buttons">
                <button class="btn--edit">
                  <img src = ./images/editButt.png  style="max-height:30px; max-width:30px" /> 
                </button>
                <button class="btn--delete">
                 <img src = ./images/delButt.png  style="max-height:30px; max-width:30px" />
                </button>
              </div>
            </td>
          </tr>
          `
                        );
                        const editBtns = tableContent.querySelectorAll(".btn--edit");
                        editBtns[editBtns.length - 1].addEventListener(
                            "click",
                            openEditStudentWindow
                        );

                        const deleteBtns = tableContent.querySelectorAll(".btn--delete");
                        deleteBtns[deleteBtns.length - 1].addEventListener(
                            "click",
                            openDeleteStudentWindow
                        );
                    } else {

                        const elements = rowToEdit.querySelectorAll("th");
                        elements[1].innerHTML = response.group;
                        elements[2].innerHTML = response.name;
                        elements[3].innerHTML = response.gender;
                        elements[4].innerHTML = response.birthday;
                    }
                } else {
                    console.log(response.errors);
                    var recieved = "";
                    if(response.errors.connection != null) recieved += response.errors.connection;
                    if(response.errors.database != null) recieved += response.errors.database;
                    if(response.errors.name != null) recieved += response.errors.name;
                    if(response.errors.gender != null) recieved += response.errors.gender;
                    if(response.errors.birthday != null) {
                        recieved += response.errors.birthday;
                    }
                    console.log(recieved.value);
                    alert(recieved);
                }
            } else {
                alert("Error: " + xhr.statusText);
            }
        };
        xhr.send(data);

        studentWindow.classList.remove("opened");
    }
}

btnAddStudentForm.addEventListener("click", addStudent);

function closeStudentWindow() {
    studentWindow.classList.remove("opened");
}


const btnCloseStudentWindow = studentWindow.querySelector(".add_cancel");


btnCloseStudentWindow.addEventListener("click", closeStudentWindow);


// EDIT STUDENT

function openEditStudentWindow(e) {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Edit student";
    rowToEdit = e.parentNode.parentNode.parentNode.parentNode;

    const elements = rowToEdit.querySelectorAll("th");
    const inputs = form.querySelectorAll("input");
    inputs[0].value = elements[2].innerText.split(" ")[0];
    inputs[1].value = elements[2].innerText.split(" ")[1];
    inputs[2].value = elements[4].innerText;
    inputs[3].value = elements[7].innerText;
    console.log(inputs[3].value);
    const selects = form.querySelectorAll("select");

    selects[0].value = elements[1].innerText;
    selects[1].value = elements[3].innerText;
}

// DELETE STUDENT
const studentDeleteWindow = document.querySelector(".delWindow");

function openDeleteStudentWindow(e) {
    studentDeleteWindow.classList.add("opened");

    rowToDelete = e.parentNode.parentNode.parentNode.parentNode;

    const name = studentDeleteWindow.querySelector("    .student-to-delete");
    console.log(name);
    const tds = rowToDelete.querySelectorAll("th");
    name.innerText = tds[2].innerText;
}

const btnDeleteStudentSubmit = studentDeleteWindow.querySelector(
    ".delStudentSubmit"
);

const btnDeleteStudentCancel = studentDeleteWindow.querySelector(
    ".delStudentCancel"
);


function closeDeleteWindow(e) {
    studentDeleteWindow.classList.remove("opened");
}

function deleteStudentSubmit(e) {
    e.preventDefault();
    var xhr = new XMLHttpRequest();
    var url = "server.php";
    var data = new FormData();
    const elements = rowToDelete.querySelectorAll("th");
    idToDeleteEdit = elements[7].innerText;
    data.append("action", "delete");
    data.append("ID",idToDeleteEdit);
    xhr.open("POST", url, true);
    xhr.onload = function () {
        if (xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.status === "success") {

                rowToDelete.outerHTML = "";
            } else {
                alert(response.errors);
            }
        } else {
            alert("Error: " + xhr.statusText);
        }
    };
    xhr.send(data);

    studentDeleteWindow.classList.remove("opened");
}

btnDeleteStudentSubmit.addEventListener("click", deleteStudentSubmit);
btnDeleteStudentCancel.addEventListener("click", closeDeleteWindow);
