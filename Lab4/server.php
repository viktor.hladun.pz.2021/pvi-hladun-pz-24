<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $conn = new mysqli("localhost", "root", "","pvi_lab4");
    $errors = array();
    if($_POST['action'] == "add" || $_POST['action'] == "edit" ){
        $group = $_POST["group"];
        $name = $_POST["name"];
        $gender = $_POST["gender"];
        $birthday = $_POST["birthday"];
        $action = $_POST["action"];

        if (empty($group)) {
            $errors["group"] = "Empty group";
        }

        if (empty($name) ) {
            $errors["name"] = "Empty first-name";
        }elseif(preg_match('~[0-9]~',$name)) {
            $errors["name"] = "Wrong name value";
        }elseif($name == " ") {
            $errors["name"] = "Wrong name value";
        }


        if (empty($gender)) {
            $errors["gender"] = "Empty gender";
        } elseif($gender != "M" && $gender != "F") {
            $errors["gender"] = "Wrong gender value";
        }
        if (empty($birthday)) {
            $errors["birthday"] = "Empty birthday";
        }
        elseif( strtotime($birthday) >= strtotime('now')){
            $errors["birthday"] = "Birthday from the future";
        }
    }

    $ID = (int)$_POST["ID"];

    if ($conn->connect_error) {
        $errors["connection"] = "Failed to setup database";
        die("Connection failed: " . $conn->connect_error);
        exit();
    }

    if ($_POST["action"] == "add") {
        if (empty($errors)) {
            $statement =  $conn->prepare("select * from students where NameStudent = ?");
            $statement->bind_param("s",$name);
            $statement->execute();
            $statement_result = $statement->get_result();
            if($statement_result->num_rows > 0){
                 $errors["database"] = "Student already exists";

            }
            else{
                    $sql = $conn->prepare("INSERT INTO  students (NameStudent, GroupStudent, Gender, Birthday) VALUES (?,?,?,?)");
                    $sql->bind_param("ssss",$name,$group,$gender,$birthday);
                    $sql->execute();
            }

        }

        if (!empty($errors)) {
            $response = array('status' => 'error', 'errors' => $errors);
        } else {
            $response = array('status' => 'success', 'group' => $group, 'name' => $name,
                "gender" => $gender, "birthday" => $birthday
            );
        }

        header('Content-Type: application/json');

        echo json_encode($response);
        $conn->close();
        exit;
    }
    elseif($_POST["action"] == "edit"){
        if (empty($errors)) {
            $statement =  $conn->prepare("select * from students where ID = ?");
            $statement->bind_param("i",$ID);
            $statement->execute();
            $statement_result = $statement->get_result();
            if($statement_result->num_rows > 0){
                $sql = $conn->prepare("UPDATE students 
          SET GroupStudent= '".$group."',
          NameStudent = '".$name."',
          Gender = '".$gender."',
          Birthday = '". $birthday ."'
          WHERE ID=$ID");

                $sql->execute();
            }
            else{
                $errors["database"] = "Student does not exist";
            }

        }

        if (!empty($errors)) {
            $response = array('status' => 'error', 'errors' => $errors);
        } else {
            $response = array('status' => 'success', 'group' => $group, 'name' => $name,
                "gender" => $gender, "birthday" => $birthday
            );
        }

        header('Content-Type: application/json');

        echo json_encode($response);
        $conn->close();
        exit;

    }
    elseif ($_POST["action"] == "delete") {


        if (empty($errors)) {
            $statement =  $conn->prepare("select * from students where ID = ?");
            $statement->bind_param("i",$ID);
            $statement->execute();
            $statement_result = $statement->get_result();
            if($statement_result->num_rows > 0) {
            $sql = $conn->prepare("DELETE FROM students
            WHERE ID = $ID");
            $sql->execute();
            }
            $response = array('status' => 'success', 'id' => $ID);
        }
        else{
            $response = array('status' => 'error', 'errors' => $errors);
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        $conn->close();
        exit;
    }
}
?>