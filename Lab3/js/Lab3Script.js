let rowToEdit;
let rowToDelete;


// ADD STUDENT

const btnAddStudent = document.querySelector(".AddButton");
const studentWindow = document.querySelector(".AddStudentWindow");

function openAddStudentWindow() {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Add student";
    form.querySelectorAll("input").forEach((input) => (input.value = ""));
    form.querySelectorAll("select").forEach((select) => (select.value = ""));
}

btnAddStudent.addEventListener("click", openAddStudentWindow);

const btnAddStudentForm = document.querySelector(".add_submit");
const form = document.querySelector(".addForm");
const tableContent = document.querySelector(".mainTable");

function addStudent(e) {
    let isFormValid = form.checkValidity();
    if (!isFormValid) {
        form.reportValidity();
    } else {
        e.preventDefault();
        form.checkValidity();
        let studentAction;
        if (studentWindow.querySelector("h3").textContent == "Add student") {
            studentAction = "add";
        } else {
            studentAction = "edit";
        }

        let group, name, gender, birthday;
        group = form.querySelector('[name="group"]').value;
        name =
            form.querySelector('[name="first-name"]').value +
            " " +
            form.querySelector('[name="last-name"]').value;
        gender = form.querySelector('[name="gender"]').value;
        birthday = form.querySelector('[name="birthday"]').value;

        var xhr = new XMLHttpRequest();
        var url = "server.php";
        var data = new FormData();
        data.append("type", "addOrEdit");
        data.append("group", group);
        data.append("name", name);
        data.append("gender", gender);
        data.append("birthday", birthday);
        xhr.open("POST", url, true);
        xhr.onload = function () {
            if (xhr.status === 200) {
                var response = JSON.parse(xhr.responseText);
                if (response.status === "success") {
                    if (studentAction === "add") {
                        tableContent.insertAdjacentHTML(
                            "beforeend",
                            `
          <tr>
            <td>
              <input type = checkbox class = "table_checkbox">
            </td>
            <td>
              ${response.group}
            </td>
            <td>
              ${response.name}
            </td>
            <td>
              ${response.gender}
            </td>
            <td>
              ${response.birthday}
            </td>
            <td>
              <input type="radio"/>
            </td>
            <td>
              <div class="option-buttons">
                <button class="btn--edit">
                  <img src = ./images/editButt.png  style="max-height:30px; max-width:30px" /> 
                </button>
                <button class="btn--delete">
                 <img src = ./images/delButt.png  style="max-height:30px; max-width:30px" />
                </button>
              </div>
            </td>
          </tr>
          `
                        );
                        const editBtns = tableContent.querySelectorAll(".btn--edit");
                        editBtns[editBtns.length - 1].addEventListener(
                            "click",
                            openEditStudentWindow
                        );

                        const deleteBtns = tableContent.querySelectorAll(".btn--delete");
                        deleteBtns[deleteBtns.length - 1].addEventListener(
                            "click",
                            openDeleteStudentWindow
                        );
                    } else {
                        const tds = rowToEdit.querySelectorAll("td");
                        tds[1].innerHTML = response.group;
                        tds[2].innerHTML = response.name;
                        tds[3].innerHTML = response.gender;
                        tds[4].innerHTML = response.birthday;
                    }
                } else {
                    alert(response.errors);
                }
            } else {
                alert("Error: " + xhr.statusText);
            }
        };
        xhr.send(data);

        studentWindow.classList.remove("opened");
    }
}

btnAddStudentForm.addEventListener("click", addStudent);

function closeStudentWindow() {
    studentWindow.classList.remove("opened");
}


const btnCloseStudentWindow = studentWindow.querySelector(".add_cancel");


btnCloseStudentWindow.addEventListener("click", closeStudentWindow);


// EDIT STUDENT

function openEditStudentWindow(e) {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Edit student";

    const currentRow =
        e.target.parentElement.parentElement.parentElement.parentElement;
    rowToEdit = currentRow;

    const tds = currentRow.querySelectorAll("td");

    console.log(currentRow);

    const inputs = form.querySelectorAll("input");

    console.log(inputs);

    inputs[0].value = tds[2].innerText.split(" ")[0];
    inputs[1].value = tds[2].innerText.split(" ")[1];
    inputs[2].value = tds[4].innerText;

    const selects = form.querySelectorAll("select");

    selects[0].value = tds[1].innerText;
    selects[1].value = tds[3].innerText;
}

// DELETE STUDENT
const studentDeleteWindow = document.querySelector(".delWindow");

function openDeleteStudentWindow(e) {
    studentDeleteWindow.classList.add("opened");
    rowToDelete = e.currentTarget.parentElement.parentElement.parentElement;
    const name = studentDeleteWindow.querySelector(".student-to-delete");
    const tds = rowToDelete.querySelectorAll("td");
    name.innerText = tds[2].innerText;
}

const btnDeleteStudentSubmit = studentDeleteWindow.querySelector(
    ".delStudentSubmit"
);

const btnDeleteStudentCancel = studentDeleteWindow.querySelector(
    ".delStudentCancel"
);


function closeDeleteWindow(e) {
    studentDeleteWindow.classList.remove("opened");
}

function deleteStudentSubmit(e) {
    e.preventDefault();
    var xhr = new XMLHttpRequest();
    var url = "server.php";
    var data = new FormData();
    data.append("type", "delete");
    xhr.open("POST", url, true);
    xhr.onload = function () {
        if (xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.status === "success") {
                console.log(rowToDelete.outerHTML);
                rowToDelete.outerHTML = "";
            } else {
                alert(response.errors);
            }
        } else {
            alert("Error: " + xhr.statusText);
        }
    };
    xhr.send(data);

    studentDeleteWindow.classList.remove("opened");
}

btnDeleteStudentSubmit.addEventListener("click", deleteStudentSubmit);
btnDeleteStudentCancel.addEventListener("click", closeDeleteWindow);
