<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST["type"] == "addOrEdit") {
        $group = $_POST["group"];
        $name = $_POST["name"];
        $gender = $_POST["gender"];
        $birthday = $_POST["birthday"];

        $errors = array();
        if (empty($group)) {
            $errors["group"] = "Empty group";
        }

        if (empty($name)) {
            $errors["name"] = "Empty first-name";
        }

        if (empty($gender)) {
            $errors["gender"] = "Empty gender";
        } elseif($gender != "Male" && $gender != "Female") {
            $errors["gender"] = "Wrong gender value";
        }

        if (empty($birthday)) {
            $errors["birthday"] = "Empty birthday";
        }

        if (!empty($errors)) {
            $response = array('status' => 'error', 'errors' => $errors);
        } else {
            $response = array('status' => 'success', 'group' => $group, 'name' => $name,
                "gender" => $gender, "birthday" => $birthday
            );
        }

        header('Content-Type: application/json');

        echo json_encode($response);
        exit;
    } elseif ($_POST["type"] == "delete") {
        $response = array('status' => 'success');
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
}
?>